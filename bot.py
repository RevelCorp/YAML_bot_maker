from hata import Client, start_clients, CHANNELS, ChannelTextBase
from hata.ext.commands import setup_ext_commands

from os import path
import toml

with open('config.toml') as f:
    config = toml.load(f)

for client in config:
    exec("{0} = Client('{1}')".format(client, config[client]['token']))
    setup_ext_commands(eval(client), config[client]['prefix'])

with open('command_handler.toml') as f:
    command_handler = toml.load(f)

for file in command_handler['files']:
    with open("{}".format(command_handler['command_path']+'/'+file)) as f:
        cmds = toml.load(f)
    for cmd in cmds:
        code = ""
        temp = list(cmds[cmd]["code"])
        for i in temp:
            if str(i) == "    ": temp[temp.index(i)] = "    "
            else: del(i);break
        if cmds[cmd]["custom_deco"] not in (None, ''):
            deco = cmds[cmd]["custom_deco"]
        else:
            deco = "@{}.commands(description='{}')".format(cmds[cmd]['client'], cmds[cmd]['description'])
        for i in temp: code = code + '    ' + i + '\n'; del(i)
        code = """{}\nasync def {}(client, message,{}):\n{}""".format(deco, cmd, str(cmds[cmd]['args'])[:-1][1:].strip('"').strip("'"), code)
        exec(code)

start_clients()